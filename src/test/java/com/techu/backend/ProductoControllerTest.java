package com.techu.backend;

import com.techu.backend.controller.ProductoController;
import org.junit.Assert;
import org.junit.jupiter.api.Test;


public class ProductoControllerTest {

    ProductoController proController = new ProductoController();

    @Test
    public void testCaseEquals() {
        // this method case passes the test
        Assert.assertEquals("API REST Tech U! v1.0.0", proController.home());
    }

    @Test
    public void testCaseNotEquals() {
        // this method case fails the test
        Assert.assertNotEquals("API REST Tech U! v1.0.0", proController.home());
    }
}
