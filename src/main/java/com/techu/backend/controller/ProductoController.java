package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.ProductoPrecioModel;
import com.techu.backend.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins="*", methods={RequestMethod.GET, RequestMethod.POST})
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("")
    public String home(){
        return "API REST Tech U! v1.0.0";
    }

    // GET all products (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.getProductos();
    }

    // GET nº de elementos del recurso productos
    @GetMapping("/productos/count")
    public long countProductos() {
       return productoService.getProductos().size();
    }

    // GET producto by ID (URI Parameters)
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoById(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) { // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(pr, HttpStatus.OK);  // return ResponseEntity.ok(pr);
    }

    // GET with URL parameters (Query Parameters)
    @GetMapping("/productos/mayores")
    public List<ProductoModel> getMayores(@RequestParam(name="pmax") int max){
        return productoService.getMayores(max);
    }

    // GET subresource
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getUsers(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if(pr == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null) {
            return ResponseEntity.ok(pr.getUsers());
        } else {
            return new ResponseEntity<>("[]", HttpStatus.OK);
        }
    }

    // POST
    @PostMapping("/productos")
    public ResponseEntity postProducto(@RequestBody ProductoModel newProduct) {
        productoService.addProducto(newProduct);
        return new ResponseEntity<>("Producto creado correctamente.", HttpStatus.CREATED);
    }

    // PUT
    @PutMapping("/productos/{id}")
    public ResponseEntity putProductoById(@PathVariable String id,
                                  @RequestBody ProductoModel productoModel){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductoById(id, productoModel);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    // PATCH actualización parcial 'precio'
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchProductoById(@PathVariable String id,
                                            @RequestBody ProductoPrecioModel productoPrecioModel){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductoPrecioById(id, productoPrecioModel.getPrecio());
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    // DELETE para borrar un productos
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProductoById(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        productoService.removeProductoById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // http code: 204
    }

//    Ejemplo para recibir una lista de parámetros en url
//    @GetMapping("/params")
//    public String getParameterList(@RequestParam(name="param") String[] paramList) {
//        String msg = "";
//        int i = 0;
//        if (paramList == null) {
//            msg = "Lista de parámetros vacía.";
//        } else {
//            for (String param : paramList) {
//                msg += "param[" + i + "] = " + param + "\n";
//                i++;
//            }
//        }
//        return msg;
//    }
}
