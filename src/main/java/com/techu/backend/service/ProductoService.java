package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.UserModel;
import org.springframework.stereotype.Component;
import java.util.ArrayList;

@Component
public class ProductoService {

    private ArrayList<ProductoModel> productoList = new ArrayList<>();

    public ProductoService() {
        productoList.add(new ProductoModel("1", "producto 1", 100.50));
        productoList.add(new ProductoModel("2", "producto 2", 150.00));
        productoList.add(new ProductoModel("3", "producto 3", 100.00));
        productoList.add(new ProductoModel("4", "producto 4", 50.50));
        productoList.add(new ProductoModel("5", "producto 5", 103.50));
        ArrayList<UserModel> users = new ArrayList<>();
        users.add(new UserModel("1"));
        users.add(new UserModel("3"));
        users.add(new UserModel("5"));
        productoList.get(1).setUsers(users);
    }

    // READ productos
    public ArrayList<ProductoModel> getProductos() {
        return productoList;
    }

    // READ instance (por ID)
    public ProductoModel getProductoById(String id) {
        if (getIndex(id)>=0){
            return productoList.get(getIndex(id));
        }
        return null;
    }

    // GET mayores que parámetro 'max'
    public ArrayList<ProductoModel> getMayores(int max){
        ArrayList<ProductoModel> mayores = new ArrayList<>();
        for (int i=0; i < productoList.size(); i++) {
            if (productoList.get(i).getPrecio() > max) {
                mayores.add(productoList.get(i));
            }
        }
        return mayores;
    }

    // CREATE
    public ProductoModel addProducto(ProductoModel nuevoProducto) {
        productoList.add(nuevoProducto);
        return nuevoProducto;
    }

    // UPDATE
    public ProductoModel updateProductoById(String id, ProductoModel newPro){
        int pos = getIndex(id);
        if (pos >= 0) {
            productoList.set(pos, newPro);
            return productoList.get(pos);
        }
        return null;
    }

    // Partial UPDATE
    public ProductoModel updateProductoPrecioById(String id, double newPrecio){
        int pos = getIndex(id);
        if (pos >= 0) {
            ProductoModel pr = productoList.get(pos);
            pr.setPrecio(newPrecio);
            productoList.set(pos, pr);
            return productoList.get(pos);
        }
        return null;
    }

    // DELETE
    public void removeProductoById(String id) {
        int pos = getIndex(id);
        if (pos >= 0) {
            productoList.remove(pos);
        }
    }

    // Devuelve la posición de un producto en productoList
    public int getIndex(String id) {
        int i = 0;
        while (i < productoList.size()) {
            if (productoList.get(i).getId().equals(id)) {
                return(i); }
            i++; }
        return -1;
    }
}
